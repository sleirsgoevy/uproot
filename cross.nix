{ nixpkgs ? import<nixpkgs>{
  crossSystem = {
    config = "aarch64-unknown-linux-gnu";
  };
  config = {
    allowUnsupportedSystem = true;
  };
}}:

(import ./default.nix { nixpkgs = nixpkgs; }).overrideAttrs(f: { LOAD_ADDR = "0x7001234000"; })

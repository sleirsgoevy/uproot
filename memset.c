void* memset(void* s, int c, unsigned long n)
{
    char* p = s;
    while(n--)
        *p++ = c;
    return s;
}

void* memcpy(void* dst, const void* src, unsigned long n)
{
    char* a = dst;
    const char* b = src;
    while(n--)
        *a++ = *b++;
    return dst;
}

LOAD_ADDR ?= 0x700001234000
NOTE_ADDR ?= 0x700001233000

ifndef IS_32BIT
CFLAGS += -mcmodel=large
endif

all: *.c payload.h
	$(CC) *.c -o payload -no-pie -fno-pic -Wl,--no-relax,-section-start=.note.gnu.property=$(NOTE_ADDR),-Ttext=$(LOAD_ADDR) -nostdlib -static -fno-stack-protector -ffreestanding $(CFLAGS) $(LDFLAGS)

clean:
	rm -f payload

#include <sys/mman.h>
#include <sys/syscall.h>
#include "payload.h"

const char* bindings_str;
struct vfs_binding* bindings;

void parse_bindings(const char* arg)
{
    size_t string_size;
    size_t nbindings = 1;
    for(string_size = 0; arg[string_size]; string_size++)
        if(arg[string_size] == ',')
            nbindings++;
    string_size++;
    char* bindings_s = (char*)do_syscall(SYS_mmap, 0, string_size, PROT_READ|PROT_WRITE, MAP_PRIVATE|MAP_ANON, -1, 0);
    for(size_t i = 0; i <= string_size; i++)
        bindings_s[i] = arg[i];
    bindings_str = bindings_s;
    struct vfs_binding* binds = (struct vfs_binding*)do_syscall(SYS_mmap, 0, sizeof(*binds)*(nbindings+1)+string_size, PROT_READ|PROT_WRITE, MAP_PRIVATE|MAP_ANON, -1, 0);
    char* s_out = (char*)(binds + nbindings + 1);
    size_t i = 0;
    nbindings = 0;
    for(;; nbindings++)
    {
        binds[nbindings].host_path = s_out;
        size_t j = i;
        while(arg[j] && arg[j] != ',' && arg[j] != ':')
            *s_out++ = arg[j++];
        j++;
        *s_out++ = 0;
        if(j == i + 2 && s_out[-2] == '/')
            s_out[-2] = 0;
        i = j;
        if(arg[j-1] != ':')
        {
            binds[nbindings].guest_path = binds[nbindings].host_path;
            binds[nbindings].flags = "";
            if(!arg[j-1])
                break;
            continue;
        }
        binds[nbindings].guest_path = s_out;
        while(arg[j] && arg[j] != ',' && arg[j] != ':')
            *s_out++ = arg[j++];
        j++;
        *s_out++ = 0;
        if(j == i + 2 && s_out[-2] == '/')
            s_out[-2] = 0;
        i = j;
        if(arg[j-1] != ':')
        {
            binds[nbindings].flags = "";
            if(!arg[j-1])
                break;
            continue;
        }
        binds[nbindings].flags = s_out;
        while(arg[j] && arg[j] != ',')
            *s_out++ = arg[j++];
        j++;
        *s_out++ = 0;
        if(!arg[j-1])
            break;
        i = j;
    }
    bindings = binds;
}

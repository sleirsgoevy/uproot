#define _GNU_SOURCE
#include <errno.h>
#include <linux/audit.h>
#include <linux/bpf.h>
#include <linux/filter.h>
#include <linux/seccomp.h>
#include <stdarg.h>
#include <stdint.h>
#include <sys/prctl.h>
#include <sys/syscall.h>
#include <unistd.h>
#include <fcntl.h>
#include <asm/sigcontext.h>
#include <asm/siginfo.h>
#include <asm/signal.h>
#include "payload.h"

#if defined(__x86_64__)
asm(".global do_syscall_asm\ndo_syscall_0:\nmov 8(%rsp), %eax\nmov %rcx, %r10\ndo_syscall_asm:\nsyscall\nbackdoor:\nret");
asm("restorer:\nmov $15, %eax\nsyscall\nret");
#elif defined(__aarch64__)
asm(".global do_syscall_asm\ndo_syscall_0:\nmov x8, x6\ndo_syscall_asm:\nsvc #0\nbackdoor:\nret");
asm("restorer:\nmov x8, #139\nsvc #0\nret");
#elif defined(__arm__)
asm(".global do_syscall_asm\ndo_syscall_0:\npush {r4, r5, r7, lr}\nldr r4, [sp, #16]\nldr r5, [sp, #20]\nldr r7, [sp, #24]\ndo_syscall_asm:\nsvc #0\nbackdoor:\npop {r4, r5, r7, pc}");
asm("restorer:\nmov r7, #119\nsvc #0\nbx lr");
#else
#error "unsupported architecture"
#endif

extern char backdoor[];
extern char restorer[];

intptr_t do_syscall_0(uintptr_t, uintptr_t, uintptr_t, uintptr_t, uintptr_t, uintptr_t, int);

intptr_t do_syscall(int nr, ...)
{
    va_list va;
    va_start(va, nr);
    uintptr_t a = va_arg(va, uintptr_t);
    uintptr_t b = va_arg(va, uintptr_t);
    uintptr_t c = va_arg(va, uintptr_t);
    uintptr_t d = va_arg(va, uintptr_t);
    uintptr_t e = va_arg(va, uintptr_t);
    uintptr_t f = va_arg(va, uintptr_t);
    va_end(va);
    return do_syscall_0(a, b, c, d, e, f, nr);
}

void hook_enosys(struct ucontext* ctx, int which)
{
    ctx->RET = -ENOSYS;
}

static syscall_hook* const hooks[] = {
#ifdef COMPAT_SYSCALLS
    [SYS_open] = hook_vfs1,
    [SYS_stat] = hook_vfs0,
    [SYS_lstat] = hook_vfsl,
    [SYS_access] = hook_vfs0,
    [SYS_mkdir] = hook_vfs0,
    [SYS_rmdir] = hook_vfs0,
    [SYS_creat] = hook_vfs0,
    [SYS_link] = hook_rename_link,
    [SYS_unlink] = hook_vfs0,
    [SYS_symlink] = hook_symlink,
    [SYS_readlink] = hook_readlink, 
    [SYS_chmod] = hook_vfs0,
    [SYS_chown] = hook_chown,
    [SYS_lchown] = hook_lchown,
    [SYS_mknod] = hook_vfs0,
    [SYS_futimesat] = hook_vfsat0,
#endif
    [SYS_getgid] = hook_getxid,
    [SYS_getuid] = hook_getxid,
    [SYS_getegid] = hook_getxid,
    [SYS_geteuid] = hook_getxid,
    [SYS_getresgid] = hook_getresxid,
    [SYS_getresuid] = hook_getresxid,
    [SYS_setgid] = hook_setxid,
    [SYS_setuid] = hook_setxid,
    [SYS_reboot] = hook_reboot,
    [SYS_setregid] = hook_setrexid,
    [SYS_setreuid] = hook_setrexid,
    [SYS_setresgid] = hook_setresxid,
    [SYS_setresuid] = hook_setresxid,
    [SYS_socket] = hook_socket,
    [SYS_connect] = hook_connect,
    [SYS_bind] = hook_bind,
    [SYS_execve] = hook_execve,
    [SYS_rt_sigprocmask] = hook_rt_sigprocmask,
    [SYS_truncate] = hook_vfs0,
    [SYS_getcwd] = hook_getcwd,
    [SYS_chdir] = hook_vfs0,
    [SYS_statfs] = hook_statfs,
    [SYS_pivot_root] = hook_enosys,
    [SYS_chroot] = hook_enosys,
    [SYS_setxattr] = hook_vfs0,
    [SYS_lsetxattr] = hook_vfsl,
    [SYS_getxattr] = hook_vfs0,
    [SYS_lgetxattr] = hook_vfsl,
    [SYS_listxattr] = hook_vfs0,
    [SYS_llistxattr] = hook_vfsl,
    [SYS_removexattr] = hook_vfs0,
    [SYS_lremovexattr] = hook_vfsl,
    [SYS_openat] = hook_openat,
    [SYS_mkdirat] = hook_vfsat0,
    [SYS_mknodat] = hook_vfsat0,
    [SYS_fchownat] = hook_fchownat,
#ifndef IS_32BIT
    [SYS_newfstatat] = hook_newfstatat,
#endif
    [SYS_unlinkat] = hook_vfsatl,
    [SYS_renameat] = hook_rename_link_at,
    [SYS_linkat] = hook_rename_link_at,
    [SYS_symlinkat] = hook_symlink,
    [SYS_readlinkat] = hook_readlink,
    [SYS_fchmodat] = hook_fchmodat,
    [SYS_faccessat] = hook_vfsat3,
    [SYS_renameat2] = hook_rename_link_at,
    [SYS_execveat] = hook_execveat,
    [SYS_statx] = hook_statx,
};

static syscall_hook* const hooks2[] = {
    [SYS_set_robust_list] = hook_fake_success,
    [SYS_accept] = hook_accept,
};

struct sock_filter bpf_pre[] = {
    {BPF_LD+BPF_W+BPF_ABS, 0, 0, 0},
    {BPF_JMP+BPF_JEQ+BPF_K, 0, 3, SYS_rt_sigaction},
    {BPF_LD+BPF_W+BPF_ABS, 0, 0, 16},
    {BPF_JMP+BPF_JEQ+BPF_K, 0, 1, SIGSYS},
    {BPF_RET+BPF_K, 0, 0, SECCOMP_RET_ERRNO+0 /* fake success */},
};

void sigsys_handler(int i, struct siginfo* si, struct ucontext* uc)
{
    if(si->si_syscall < sizeof(hooks) / sizeof(*hooks) && hooks[si->si_syscall])
        hooks[si->si_syscall](uc, si->si_syscall);
    else if(si->si_syscall < sizeof(hooks2) / sizeof(*hooks2) && hooks2[si->si_syscall])
        hooks2[si->si_syscall](uc, si->si_syscall);
    else
        uc->RET = -ENOSYS;
}

void sigsys_test(int i, struct siginfo* si, struct ucontext* uc)
{
    uc->RET = -ENOSYS;
}

#if defined(__x86_64__)
__attribute__((naked)) void _start(void)
{
    asm volatile(
        "mov %rsp, %rdi\n"
        "and $0xfffffffffffffff0, %rsp\n"
        "call entry"
    );
}
#elif defined(__aarch64__)
asm(".global _start\n_start:\nmov x0, sp\nbl entry");
#elif defined(__arm__)
asm(".global _start\n_start:\nmov r0, sp\nbl entry");
#else
#error "unsupported architecture"
#endif

int have_statx = 1;

void entry(void* stack, uintptr_t zero, void* rdx)
{
    struct sigaction sa = {
        .sa_handler = (void*)sigsys_test,
        .sa_flags = SA_SIGINFO|SA_RESTORER,
        .sa_restorer = (void*)restorer,
    };
    do_syscall(SYS_rt_sigaction, SIGSYS, &sa, 0, sizeof(sigset_t));
    if(do_syscall(SYS_statx, AT_FDCWD, 0, 0, 0, 0) == -ENOSYS)
        have_statx = 0;
    sa.sa_handler = (void*)sigsys_handler;
    do_syscall(SYS_rt_sigaction, SIGSYS, &sa, 0, sizeof(sigset_t));
    int nsys = 0;
    for(int i = 0; i < sizeof(hooks) / sizeof(*hooks); i++)
        if(hooks[i])
            nsys++;
    size_t npre = sizeof(bpf_pre) / sizeof(*bpf_pre);
    struct sock_filter instrs[nsys+npre+10];
#ifdef IS_32BIT
    uint32_t syscall_backdoor = (uint32_t)&backdoor;
    instrs[2] = (struct sock_filter){BPF_LD+BPF_W+BPF_ABS, 0, 0, 8};
    instrs[3] = (struct sock_filter){BPF_JMP+BPF_JEQ+BPF_K, 0, 1, syscall_backdoor};
#else
    uint64_t syscall_backdoor = (uint64_t)&backdoor;
    instrs[0] = (struct sock_filter){BPF_LD+BPF_W+BPF_ABS, 0, 0, 8};
    instrs[1] = (struct sock_filter){BPF_JMP+BPF_JEQ+BPF_K, 0, 3, syscall_backdoor};
    instrs[2] = (struct sock_filter){BPF_LD+BPF_W+BPF_ABS, 0, 0, 12};
    instrs[3] = (struct sock_filter){BPF_JMP+BPF_JEQ+BPF_K, 0, 1, syscall_backdoor >> 32};
#endif
    instrs[4] = (struct sock_filter){BPF_RET+BPF_K, 0, 0, SECCOMP_RET_ALLOW};
    instrs[5] = (struct sock_filter){BPF_LD+BPF_W+BPF_ABS, 0, 0, 4};
    instrs[6] = (struct sock_filter){BPF_JMP+BPF_JEQ+BPF_K, 0, nsys+npre+2, AUDIT_ARCH_NATIVE};
    for(size_t i = 0; i < npre; i++)
        instrs[i+7] = bpf_pre[i];
    instrs[npre+7] = (struct sock_filter){BPF_LD+BPF_W+BPF_ABS, 0, 0, 0};
    int idx = 0;
    for(int i = 0; i < sizeof(hooks) / sizeof(*hooks); i++)
        if(hooks[i])
        {
            instrs[idx+npre+8] = (struct sock_filter){BPF_JMP+BPF_JEQ+BPF_K, nsys-idx, 0, i};
            idx++;
        }
    instrs[nsys+npre+8] = (struct sock_filter){BPF_RET+BPF_K, 0, 0, SECCOMP_RET_ALLOW};
    instrs[nsys+npre+9] = (struct sock_filter){BPF_RET+BPF_K, 0, 0, SECCOMP_RET_TRAP};
#ifdef IS_32BIT
    struct sock_fprog prg = {nsys+npre+8, instrs+2};
#else
    struct sock_fprog prg = {nsys+npre+10, instrs};
#endif
    continue_execve(stack, rdx, &prg);
}

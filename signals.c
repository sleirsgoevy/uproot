#include <unistd.h>
#include <fcntl.h>
#include <sys/syscall.h>
#include <sys/errno.h>
#include "payload.h"

#if 0

void hook_rt_sigprocmask(struct ucontext* ctx, int which)
{
    //prevent blocking SIGSYS
    int how = ctx->ARG0;
    const unsigned char* orig_mask = (const unsigned char*)ctx->ARG1;
    unsigned char* new_mask = (unsigned char*)ctx->ARG2;
    size_t sz = ctx->ARG3;
    if((how != SIG_BLOCK && how != SIG_SETMASK)
    || !orig_mask || !(orig_mask[SIGSYS>>3] & (1 << (SIGSYS & 7))))
    {
        ctx->RET = do_syscall(SYS_rt_sigprocmask, how, orig_mask, new_mask, sz);
        return;
    }
    unsigned char mod_mask[sz];
    for(size_t i = 0; i < sz; i++)
        mod_mask[i] = orig_mask[i];
    mod_mask[SIGSYS>>3] &= ~(1 << (SIGSYS & 7));
    ctx->RET = do_syscall(SYS_rt_sigprocmask, how, mod_mask, new_mask, sz);
}

#else

void hook_rt_sigprocmask(struct ucontext* ctx, int which)
{
    int how = ctx->ARG0;
    const unsigned char* new_mask = (const unsigned char*)ctx->ARG1;
    unsigned char* old_mask = (unsigned char*)ctx->ARG2;
    unsigned char* cur_mask = (unsigned char*)&ctx->uc_sigmask;
    size_t sz = ctx->ARG3;
    if(sz != 8)
    {
        ctx->RET = -EINVAL;
        return;
    }
    unsigned char tmp_old_mask[sz];
    for(size_t i = 0; i < sz; i++)
        tmp_old_mask[i] = cur_mask[i];
    if(new_mask)
    {
        switch(how)
        {
        case SIG_BLOCK:
            for(size_t i = 0; i < sz; i++)
                cur_mask[i] |= new_mask[i];
            break;
        case SIG_UNBLOCK:
            for(size_t i = 0; i < sz; i++)
                cur_mask[i] &= ~new_mask[i];
            break;
        case SIG_SETMASK:
            for(size_t i = 0; i < sz; i++)
                cur_mask[i] = new_mask[i];
            break;
        default:
            ctx->RET = -EINVAL;
            return;
        }
        cur_mask[(SIGSYS-1) >> 3] &= ~(1 << ((SIGSYS-1) & 7));
    }
    if(old_mask)
        for(size_t i = 0; i < sz; i++)
            old_mask[i] = tmp_old_mask[i];
    ctx->RET = 0;
}

#endif

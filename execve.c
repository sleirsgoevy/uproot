#define stat kstat
#include <asm/stat.h>
#undef stat
#include <elf.h>
#include <linux/seccomp.h>
#include <stddef.h>
#include <stdint.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/auxv.h>
#include <sys/errno.h>
#include <sys/mman.h>
#include <sys/prctl.h>
#include <sys/syscall.h>
#include <sys/user.h>
#include "payload.h"

const char* exe_path;

int is_executable(struct kstat* kst)
{
    uid_t uid = do_syscall(SYS_getuid);
    if(!uid)
        return kst->st_mode & 0111;
    else if(uid == kst->st_uid)
        return kst->st_mode & 0100;
    if(do_syscall(SYS_getgid) == kst->st_gid)
        return kst->st_mode & 0010;
    int ngroups = do_syscall(SYS_getgroups, 0, (void*)0);
    if(ngroups >= 0)
    {
        int groups[ngroups];
        if(do_syscall(SYS_getgroups, ngroups, groups) == ngroups)
        {
            for(int i = 0; i < ngroups; i++)
                if(kst->st_gid == groups[i])
                    return kst->st_mode & 0010;
        }
    }
    return kst->st_mode & 0001;
}

int check_elf(int fd, int* has_interp, Path interp)
{
    ElfNative_Ehdr hdr = {0};
    do_syscall(SYS_lseek, fd, 0, SEEK_SET);
    do_syscall(SYS_read, fd, &hdr, sizeof(hdr));
    for(size_t i = 0; i < SELFMAG; i++)
        if(hdr.e_ident[i] != ELFMAG[i])
            return -1;
    if(hdr.e_ident[EI_CLASS] != ELFCLASSNATIVE)
        return -1;
    if(hdr.e_ident[EI_DATA] != ELFDATA2LSB)
        return -1;
    if(hdr.e_ident[EI_OSABI] != ELFOSABI_NONE && hdr.e_ident[EI_OSABI] != ELFOSABI_GNU)
        return -1;
    if(hdr.e_machine != EM_NATIVE)
        return -1;
    do_syscall(SYS_lseek, fd, hdr.e_phoff, SEEK_SET);
    for(size_t i = 0; i < hdr.e_phnum; i++)
    {
        ElfNative_Phdr phdr = {0};
        do_syscall(SYS_read, fd, &phdr, sizeof(phdr));
        if(phdr.p_type == PT_INTERP)
        {
            do_syscall(SYS_lseek, fd, phdr.p_offset, SEEK_SET);
            ssize_t sz = do_syscall(SYS_read, fd, interp, MAX_PATH_LEN);
            if((*has_interp = sz > 0) && sz < MAX_PATH_LEN)
                interp[sz] = 0;
            return 0;
        }
    }
    *has_interp = 0;
    return 0;
}

void die(void)
{
    for(;;)
        do_syscall(SYS_kill, do_syscall(SYS_getpid), SIGTRAP);
}

void load_elf(int fd, uintptr_t* phdr_addr, size_t* phnum, uintptr_t* entry_addr, uintptr_t* base_addr)
{
    ElfNative_Ehdr ehdr = {0};
    do_syscall(SYS_lseek, fd, 0, SEEK_SET);
    do_syscall(SYS_read, fd, &ehdr, sizeof(ehdr));
    do_syscall(SYS_lseek, fd, ehdr.e_phoff, SEEK_SET);
    *phnum = ehdr.e_phnum;
    uintptr_t low_addr = -1;
    uintptr_t high_addr = 0;
    int have_phdr = 0;
    for(size_t i = 0; i < ehdr.e_phnum; i++)
    {
        ElfNative_Phdr phdr = {0};
        do_syscall(SYS_read, fd, &phdr, sizeof(phdr));
        switch(phdr.p_type)
        {
        case PT_PHDR:
            have_phdr = 1;
        case PT_LOAD:
        {
            if(phdr.p_vaddr < low_addr)
                low_addr = phdr.p_vaddr;
            if(phdr.p_vaddr + phdr.p_memsz > high_addr)
                high_addr = phdr.p_vaddr + phdr.p_memsz;
        }
        }
    }
    low_addr &= (uintptr_t)-PAGE_SIZE;
    uintptr_t base;
    if(ehdr.e_type == ET_EXEC)
    {
        base = 0;
        uintptr_t mapping = do_syscall(SYS_mmap, low_addr, high_addr - low_addr, PROT_NONE, MAP_PRIVATE|MAP_ANON, -1, 0);
        if(mapping != low_addr)
            die();
    }
    else
    {
        base = do_syscall(SYS_mmap, 0, high_addr - low_addr, PROT_NONE, MAP_PRIVATE|MAP_ANON, -1, 0);
        if(base > (uintptr_t)-4096)
            die();
        base -= low_addr;
    }
    *base_addr = base;
    *entry_addr = base + ehdr.e_entry;
    do_syscall(SYS_lseek, fd, ehdr.e_phoff, SEEK_SET);
    for(size_t i = 0; i < ehdr.e_phnum; i++)
    {
        ElfNative_Phdr phdr = {0};
        do_syscall(SYS_read, fd, &phdr, sizeof(phdr));
        switch(phdr.p_type)
        {
        case PT_PHDR:
            *phdr_addr = phdr.p_vaddr + base;
            break;
        case PT_LOAD:
        {
            uintptr_t vaddr = phdr.p_vaddr + base;
            uintptr_t offset = phdr.p_offset;
            uintptr_t sz = phdr.p_memsz;
            if((vaddr & (PAGE_SIZE - 1)) != (offset & (PAGE_SIZE - 1)))
                die();
            sz += (vaddr & (PAGE_SIZE - 1));
            vaddr &= (uintptr_t)-PAGE_SIZE;
            offset &= (uintptr_t)-PAGE_SIZE;
            uintptr_t sz0 = (sz + phdr.p_filesz - phdr.p_memsz + PAGE_SIZE - 1) & (uintptr_t)-PAGE_SIZE;
            sz = (sz + PAGE_SIZE - 1) & (uintptr_t)-PAGE_SIZE;
            uintptr_t mapping = do_syscall(SYS_mmap, vaddr, sz0, PROT_READ|PROT_WRITE|PROT_EXEC, MAP_PRIVATE|MAP_FIXED, fd, offset);
            if(mapping != vaddr)
                die();
            if(sz != sz0 && do_syscall(SYS_mmap, vaddr+sz0, sz-sz0, PROT_READ|PROT_WRITE|PROT_EXEC, MAP_PRIVATE|MAP_FIXED|MAP_ANON, -1, 0) != vaddr + sz0)
                die();
            char* bss_start = (char*)(phdr.p_vaddr + base + phdr.p_filesz);
            char* bss_end = (char*)(vaddr + sz);
            for(char* i = bss_start; i < bss_end; i++)
                *i = 0;
            int prot = 0;
            if((phdr.p_flags & PF_R))
                prot |= PROT_READ;
            if((phdr.p_flags & PF_W))
                prot |= PROT_WRITE;
            if((phdr.p_flags & PF_X))
                prot |= PROT_EXEC;
            if(do_syscall(SYS_mprotect, vaddr, sz, prot))
                die();
        }
        }
    }
    if(!have_phdr)
        *phdr_addr = 0;
}

static int start_execve(int dirfd, const char* path, const char** argv, const char** envp, int is_script)
{
    struct kstat st;
    //XXX: abstraction leak
    Path host_path; //need to pass this to the child
    int error = translate_path(host_path, dirfd, path);
    if(error)
        return error;
    int fd = do_syscall(SYS_openat, dirfd, host_path, O_RDONLY);
    if(fd < 0)
        return fd;
    if(do_syscall(SYS_fstat, fd, &st) || !is_executable(&st))
    {
        do_syscall(SYS_close, fd);
        return -EACCES;
    }
    int has_interp;
    Path interp;
    char shell_magic[2] = {0};
    do_syscall(SYS_lseek, fd, 0, SEEK_SET);
    do_syscall(SYS_read, fd, shell_magic, 2);
    //unfortunately recursive shebangs are a thing
    if(/*!is_script && */shell_magic[0] == '#' && shell_magic[1] == '!') //script sheband
    {
        char* p = interp;
        size_t sz = sizeof(interp);
        while(sz)
        {
            ssize_t chk = do_syscall(SYS_read, fd, p, sz);
            if(chk < 0)
            {
                do_syscall(SYS_close, fd);
                return -EIO;
            }
            if(!chk)
                break;
            p += chk;
            sz -= chk;
        }
        do_syscall(SYS_close, fd);
        sz = p - interp;
        size_t i;
        for(i = 0; i < sz && interp[i] && interp[i] != '\n'; i++);
        if(i == sizeof(interp))
            return -ENOMEM;
        if(i < sz && !interp[i])
            return -ENOEXEC;
        interp[i] = 0;
        char* interp_arg = 0;
        for(i = 0; interp[i] == ' '; i++);
        for(; interp[i] && interp[i] != ' '; i++);
        if(interp[i] == ' ')
        {
            interp[i] = 0;
            interp_arg = interp + i + 1;
        }
        size_t offset = interp_arg ? 2 : 1;
        size_t argc;
        for(argc = 0; argv[argc]; argc++);
        const char* interp_p = interp;
        while(*interp_p == ' ')
            interp_p++;
        const char* argv2[offset+argc+1];
        argv2[0] = interp_p;
        argv2[1] = interp_arg;
        argv2[offset] = path;
        for(size_t i = 1; i <= argc; i++)
            argv2[offset+i] = argv[i];
        return start_execve(AT_FDCWD, interp_p, argv2, envp, 1);
    }
    if(check_elf(fd, &has_interp, interp))
    {
        do_syscall(SYS_close, fd);
        return -ENOEXEC;
    }
    int interp_fd = fd;
    if(has_interp)
    {
        interp_fd = vfs_openat(AT_FDCWD, interp, O_RDONLY);
        if(interp_fd < 0)
        {
            do_syscall(SYS_close, fd);
            return interp_fd;
        }
        if(check_elf(interp_fd, &has_interp, interp) || has_interp)
        {
            do_syscall(SYS_close, interp_fd);
            do_syscall(SYS_close, fd);
            return -ENOEXEC;
        }
    }
    char fd1[16], fd2[16];
    char* p_fd1 = fd1;
    char* p_fd2 = fd2;
    outn(&p_fd1, fd);
    outn(&p_fd2, interp_fd);
    size_t nargs;
    for(nargs = 0; argv[nargs]; nargs++);
    const char* new_argv[nargs+7];
    //note: must be even number to maintain stack alignment
    new_argv[0] = "/fd_runner";
    new_argv[1] = fd1;
    new_argv[2] = fd2;
    new_argv[3] = bindings_str;
    new_argv[4] = host_path;
    char f_id0[2] = {fake_id0 + '0', 0};
    new_argv[5] = f_id0;
    for(size_t i = 0; i <= nargs; i++)
        new_argv[6+i] = argv[i];
    int err = do_syscall(SYS_execve, "/proc/self/exe", new_argv, envp);
    do_syscall(SYS_close, fd2);
    do_syscall(SYS_close, fd);
    return err;
}

void hook_execve(struct ucontext* ctx, int which)
{
    do_syscall(SYS_rt_sigprocmask, SIG_SETMASK, (void*)&ctx->uc_sigmask, 0, 8);
    ctx->RET = start_execve(AT_FDCWD, (const char*)ctx->ARG0, (const char**)ctx->ARG1, (const char**)ctx->ARG2, 0);
}

void hook_execveat(struct ucontext* ctx, int which)
{
    do_syscall(SYS_rt_sigprocmask, SIG_SETMASK, (void*)&ctx->uc_sigmask, 0, 8);
    ctx->RET = start_execve(ctx->ARG0, (const char*)ctx->ARG1, (const char**)ctx->ARG2, (const char**)ctx->ARG3, 0);
}

static const char USAGE[] = R"(usage: uproot <chroot> <executable> [args]

Runs the specified path in a fake chroot.
)";

static int parse_int(const char* s)
{
    int ans = 0;
    while(*s)
        ans = 10 * ans + (*s++) - '0';
    return ans;
}

#if defined(__x86_64__)
__attribute__((naked)) void pivot(void* stack, void* entry, void* rdx)
{
    asm volatile("mov %rdi, %rsp\njmp *%rsi");
}
#elif defined(__aarch64__)
void pivot(void* stack, void* entry, void* rdx);
asm("pivot:\nmov sp, x0\nbr x1");
#elif defined(__arm__)
void pivot(void* stack, void* entry, void* rdx);
asm("pivot:\nmov sp, r0\nbx r1");
#else
#error "unsupported architecture"
#endif

void continue_execve(uintptr_t* stack, void* rdx, void* bpf)
{
    int argc = stack[0];
    const char** argv = (const char**)(stack + 1);
    char* argv_start = (char*)argv[0];
    const char** envp = argv + argc + 1;
    uintptr_t* auxv = (void*)envp;
    while(*auxv)
        auxv++;
    auxv++;
    int mode = 1;
    for(size_t i = 0; i < 11; i++)
        if(argv[0][i] != "/fd_runner"[i])
            mode = 0;
    if(mode == 0) //from shell
    {
        do_syscall(SYS_prctl, PR_SET_NO_NEW_PRIVS, 1, 0, 0, 0);
        do_syscall(SYS_prctl, PR_SET_SECCOMP, SECCOMP_MODE_FILTER, bpf);
        if(argc >= 2)
        {
            size_t i;
            for(i = 0; i < 6 && argv[1][i] == "--id0"[i]; i++);
            if(i == 6)
            {
                fake_id0 = 1;
                argc--;
                argv++;
            }
        }
        if(argc < 3)
        {
            do_syscall(SYS_write, 1, USAGE, sizeof(USAGE) - 1);
            do_syscall(SYS_exit_group, 0);
        }
        parse_bindings(argv[1]);
        do_syscall(SYS_setsid);
        int err = start_execve(AT_FDCWD, argv[2], argv+2, envp, 0);
        char buf[64] = "execve error, errno = ";
        char* o = buf + 22;
        outn(&o, -err);
        *o = '\n';
        do_syscall(SYS_write, 2, buf, o + 1 - buf);
        do_syscall(SYS_exit_group, 1);
    }
    //otherwise we are /fd_runner
    int fd_main = parse_int(argv[1]);
    int fd_interp = parse_int(argv[2]);
    parse_bindings(argv[3]);
    const char* exe_pth = argv[4];
    fake_id0 = argv[5][0] - '0';
    size_t exe_pth_sz;
    for(exe_pth_sz = 0; exe_pth[exe_pth_sz]; exe_pth_sz++);
    exe_pth_sz++;
    char* exe_p = (char*)do_syscall(SYS_mmap, 0, exe_pth_sz, PROT_READ|PROT_WRITE, MAP_PRIVATE|MAP_ANON, -1, 0);
    for(size_t i = 0; i <= exe_pth_sz; i++)
        exe_p[i] = exe_pth[i];
    exe_path = exe_p;
    char* argv_mid = (char*)argv[6];
    char* argv_end = (char*)argv[argc-1];
    while(*argv_end)
        argv_end++;
    argv_end++;
    for(size_t i = 0; i < argv_end - argv_mid; i++)
        argv_start[i] = argv_mid[i];
    for(size_t i = argv_end - argv_mid; i < argv_end - argv_start; i++)
        argv_start[i] = 0;
    argv += 6;
    argc -= 6;
    stack = (uintptr_t*)(argv-1);
    stack[0] = argc;
    for(size_t i = 0; i < argc; i++)
        argv[i] -= argv_mid - argv_start;
    uintptr_t entry, phdr, tmp;
    size_t phnum;
    load_elf(fd_main, &phdr, &phnum, &entry, &tmp);
    uintptr_t real_entry = entry;
    uintptr_t base = 0;
    if(fd_interp != fd_main)
        load_elf(fd_interp, &tmp, &tmp, &real_entry, &base);
    do_syscall(SYS_close, fd_main);
    do_syscall(SYS_close, fd_interp);
    for(; *auxv; auxv += 2)
    {
        switch(auxv[0])
        {
        case AT_BASE:
            auxv[1] = base;
            break;
        case AT_ENTRY:
            auxv[1] = entry;
            break;
        case AT_PHDR:
            auxv[1] = phdr;
            break;
        case AT_PHNUM:
            auxv[1] = phdr ? phnum : 0;
        }
    }
    pivot(stack, (void*)real_entry, rdx);
}

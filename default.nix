{ nixpkgs ? import<nixpkgs>{} }:

nixpkgs.stdenv.mkDerivation {
  name = "uproot";
  src = ./.;
  installPhase = ''
    mkdir -p $out/bin
    cp payload $out/bin/uproot
  '';
}

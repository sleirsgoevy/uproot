#include <unistd.h>
#include <fcntl.h>
#include <sys/syscall.h>
#include <sys/errno.h>
#include "payload.h"

//workarounds for android's seccomp filters
//no real value added

void hook_fake_success(struct ucontext* ctx, int which)
{
    ctx->RET = 0;
}

void hook_accept(struct ucontext* ctx, int which)
{
    ctx->RET = do_syscall(SYS_accept4, ctx->ARG0, ctx->ARG1, ctx->ARG2, 0);
}

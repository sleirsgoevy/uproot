#define _GNU_SOURCE
#define _SYS_SELECT_H
#include <unistd.h>
#include <fcntl.h>
#include <sys/syscall.h>
#include <sys/errno.h>
#include <sys/socket.h>
#include <sys/un.h>
#include "payload.h"

static int translate_local_unix(struct sockaddr_un* out, struct sockaddr_un* in, socklen_t* sl, int* fd)
{
    if(!in->sun_path[0]) //abstract name
    {
        *out = *in;
        *fd = -1;
        return 0;
    }
    char sun_path[sizeof(in->sun_path)];
    size_t i;
    for(i = 0; i < sizeof(sun_path) && in->sun_path[i]; i++)
        sun_path[i] = in->sun_path[i];
    if(i == sizeof(sun_path))
        return -EINVAL;
    size_t fp = i;
    while(fp && sun_path[fp] != '/')
        fp--;
    if(!fp) //location in cwd
    {
        *out = *in;
        *fd = -1;
        return 0;
    }
    fp++;
    char fn1 = sun_path[fp];
    sun_path[fp] = 0;
    Path host_path;
    int error;
    if((error = translate_path(host_path, AT_FDCWD, sun_path)))
        return error;
    sun_path[fp] = fn1;
#ifdef COMPAT_SYSCALLS
    int dirfd = do_syscall(SYS_open, host_path, O_PATH);
#else
    int dirfd = do_syscall(SYS_openat, AT_FDCWD, host_path, O_PATH);
#endif
    if(dirfd < 0)
        return dirfd;
    out->sun_family = AF_UNIX;
    // /dev/fd is shorter than /proc/self/fd
    char* p = out->sun_path;
    for(size_t j = 0; j < 8; j++)
        *p++ = "/dev/fd/"[j];
    outn(&p, dirfd);
    *p++ = '/';
    for(size_t j = fp; p - out->sun_path < sizeof(out->sun_path) && j < i; j++)
        *p++ = sun_path[j];
    if(p - out->sun_path + 1 >= sizeof(out->sun_path))
    {
        do_syscall(SYS_close, dirfd);
        return -ENAMETOOLONG; //TODO
    }
    *p++ = 0;
    *fd = dirfd;
    *sl = p - (char*)out;
    return 0;
}

static int translate_local(struct sockaddr* out, struct sockaddr* in, socklen_t* sl, int* fd)
{
    if(in->sa_family == AF_UNIX)
        return translate_local_unix((struct sockaddr_un*)out, (struct sockaddr_un*)in, sl, fd);
    else
    {
        *fd = -1;
        *out = *in;
        return 0;
    }
}

static int translate_remote_unix(struct sockaddr_un* out, struct sockaddr_un* in, socklen_t* sl, int* fd)
{
    if(!in->sun_path[0]) //abstract name
    {
        *out = *in;
        *fd = -1;
        return 0;
    }
    size_t i;
    for(i = 0; i < sizeof(in->sun_path) && in->sun_path[i]; i++);
    if(i == sizeof(in->sun_path))
        return -EINVAL;
    Path host_path;
    int error;
    if((error = translate_path(host_path, AT_FDCWD, in->sun_path)))
        return error;
#ifdef COMPAT_SYSCALLS
    int pathfd = do_syscall(SYS_open, host_path, O_PATH);
#else
    int pathfd = do_syscall(SYS_openat, AT_FDCWD, host_path, O_PATH);
#endif
    if(pathfd < 0)
        return -ECONNREFUSED;
    out->sun_family = AF_UNIX;
    char* p = out->sun_path;
    for(size_t i = 0; i < 14; i++)
        *p++ = "/proc/self/fd/"[i];
    outn(&p, pathfd);
    *p++ = 0;
    *sl = p - (char*)out;
    *fd = pathfd;
    return 0;
}

static int translate_remote(struct sockaddr* out, struct sockaddr* in, socklen_t* sl, int* fd)
{
    if(in->sa_family == AF_UNIX)
        return translate_remote_unix((struct sockaddr_un*)out, (struct sockaddr_un*)in, sl, fd);
    else
    {
        *fd = -1;
        *out = *in;
        return 0;
    }
}

void hook_socket(struct ucontext* ctx, int which)
{
    if(ctx->ARG0 == AF_UNIX && ctx->ARG1 != SOCK_STREAM)
        //too cumbersome to support
        ctx->RET = -EINVAL;
    else
        ctx->RET = do_syscall(SYS_socket, ctx->ARG0, ctx->ARG1, ctx->ARG2);
}

void hook_bind(struct ucontext* ctx, int which)
{
    struct sockaddr_un addr; //the largest one
    int error, fd;
    socklen_t sl = ctx->ARG2;
    if((error = translate_local((struct sockaddr*)&addr, (struct sockaddr*)ctx->ARG1, &sl, &fd)))
        ctx->RET = error;
    else
    {
        ctx->RET = do_syscall(SYS_bind, ctx->ARG0, (struct sockaddr*)&addr, sl);
        if(fd >= 0)
            do_syscall(SYS_close, fd);
    }
}

void hook_connect(struct ucontext* ctx, int which)
{
    struct sockaddr_un addr; //the largest one
    int error, fd;
    socklen_t sl = ctx->ARG2;
    if((error = translate_remote((struct sockaddr*)&addr, (struct sockaddr*)ctx->ARG1, &sl, &fd)))
        ctx->RET = error;
    else
    {
        ctx->RET = do_syscall(SYS_connect, ctx->ARG0, (struct sockaddr*)&addr, sl);
        if(fd >= 0)
            do_syscall(SYS_close, fd);
    }
}

//TODO: getpeername, getsockname

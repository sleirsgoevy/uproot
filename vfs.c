#define flock not_flock
#include <linux/fcntl.h>
#undef flock
#include <linux/magic.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/syscall.h>
#include <sys/errno.h>
#include <sys/vfs.h>
#include "payload.h"

int concat_paths(Path out, const char* p1, const char* p2)
{
    size_t i, j;
    for(i = 0; i < MAX_PATH_LEN && p1[i]; i++)
        out[i] = p1[i];
    if(i == MAX_PATH_LEN)
        return -1;
    if(i && out[i-1] != '/')
    {
        out[i++] = '/';
        if(i == MAX_PATH_LEN)
            return -1;
    }
    int has_non_slash = 0;
    for(j = 0; p2[j];)
    {
        if(p2[j] != '/')
            has_non_slash = 1;
        if(p2[j] == '/' || p2[j] == '.' && (!p2[j+1] || p2[j+1] == '/'))
        {
            p2++;
            continue;
        }
        if(i && *out == '/' && p2[j] == '.' && p2[j+1] == '.' && (!p2[j+2] || p2[j+2] == '/'))
        {
            if(i > 1)
                i--;
            while(i > 1 && out[i-1] != '/')
                i--;
            p2++;
            continue;
        }
        while(i < MAX_PATH_LEN && p2[j] && p2[j] != '/')
            out[i++] = p2[j++];
        if(i + 1 >= MAX_PATH_LEN)
            return -1;
        out[i++] = '/';
    }
    //trailing slash IS significant if the target is a symlink to directory
    if(i > 1 && out[i-1] == '/' && !(has_non_slash && p2[j-1] == '/'))
        i--;
    out[i] = 0;
    return 0;
}

void outn(char** out, unsigned int n)
{
    if(n >= 10)
        outn(out, n / 10);
    **out = n % 10 + '0';
    *++*out = 0;
}

//XXX: slow algorithm
#define FIND_BINDING(func, field) \
struct vfs_binding* func(const char* path, size_t* sz)\
{\
    struct vfs_binding* best = 0;\
    size_t best_len = 0;\
    for(size_t i = 0; bindings[i].field; i++)\
    {\
        const char* path2 = bindings[i].field;\
        size_t j;\
        for(j = 0; path2[j] && path2[j] == path[j]; j++);\
        if(!path2[j] && (!path[j] || path[j] == '/') && j >= best_len)\
        {\
            best_len = j;\
            best = bindings + i;\
        }\
    }\
    *sz = best_len;\
    return best;\
}

FIND_BINDING(find_binding_by_host_path, host_path)
FIND_BINDING(find_binding_by_guest_path, guest_path)

__attribute__((optimize(0)))
size_t translate_back(const Path in, char* buf, size_t sz, int* status)
{
    size_t i;
    struct vfs_binding* b = find_binding_by_host_path(in, &i);
    if(!b) //path not from chroot, unreachable
    {
        while(i < MAX_PATH_LEN && in[i])
            i++;
        if(i < MAX_PATH_LEN)
            i++;
        if(sz > i + 13)
            sz = i + 13;
        for(size_t j = 13; j < sz; j++)
            buf[j] = in[j - 13];
        for(size_t j = 0; j < 13 && j < sz; j++)
            buf[j] = "(unreachable)"[j];
        *status = 1;
        return sz;
    }
    else
    {
        size_t j;
        for(j = 0; j < sz && b->guest_path[j]; j++)
            buf[j] = b->guest_path[j];
        if(j == sz)
            return sz;
        for(size_t k = i; j < sz && in[k]; k++)
            buf[j++] = in[k];
        if(j == sz)
            return sz;
        if(j == 0)
            buf[j++] = '/';
        if(j == sz)
            return sz;
        buf[j++] = 0;
        *status = 0;
        return j;
    }
}

enum { TRPTH_GUEST_ROOT = 1, TRPTH_NO_PROC_SELF_EXE };

static int translate_path_internal(Path out, int dirfd, const char* filename, int flags, struct vfs_binding** binding)
{
    if(!*filename)
        return -ENOENT; //empty path is always invalid, catch it early
    if(binding)
        *binding = 0;
    Path vfs_path;
    if(*filename == '/')
    {
        if(concat_paths(vfs_path, "/", filename))
            return -ENOMEM;
    }
    else
    {
        Path cwd0;
        if(dirfd != AT_FDCWD)
        {
            Path proc_self_fd;
            for(size_t i = 0; i < 14; i++)
                proc_self_fd[i] = "/proc/self/fd/"[i];
            char* dst = proc_self_fd + 14;
            outn(&dst, dirfd);
#ifdef COMPAT_SYSCALLS
            ssize_t sz = do_syscall(SYS_readlink, proc_self_fd, cwd0, MAX_PATH_LEN);
#else
            ssize_t sz = do_syscall(SYS_readlinkat, AT_FDCWD, proc_self_fd, cwd0, MAX_PATH_LEN);
#endif
            if(sz < 0 || sz == MAX_PATH_LEN)
                return -EBADF;
            cwd0[sz] = 0;
        }
        else
        {
            ssize_t sz = do_syscall(SYS_getcwd, cwd0, MAX_PATH_LEN);
            if(sz < 0 || sz == MAX_PATH_LEN) //XXX: cannot get cwd
                *cwd0 = 0;
        }
        int status = 1;
        Path cwd;
        if(*cwd0)
            translate_back(cwd0, cwd, MAX_PATH_LEN, &status);
        if(status) //unreachable path, using original relative
        {
            *cwd = 0;
            flags |= TRPTH_GUEST_ROOT;
        }
        if(concat_paths(vfs_path, cwd, filename))
            return -ENOMEM;
    }
    if((flags & TRPTH_GUEST_ROOT))
    {
        for(size_t i = 0; i < MAX_PATH_LEN && (!i || vfs_path[i-1]); i++)
            out[i] = vfs_path[i];
    }
    else
    {
        size_t i;
        struct vfs_binding* b = find_binding_by_guest_path(vfs_path, &i);
        if(!b)
            return -ENOENT;
        if(concat_paths(out, (*b->host_path) ? b->host_path : "/", vfs_path+i))
            return -ENOMEM;
        if(!*out)
        {
            out[0] = '/';
            out[1] = 0;
        }
        //XXX: /proc/self/exe is now a file and not a symlink
        //readlink has an explicit check for this
        if(!(flags & TRPTH_NO_PROC_SELF_EXE))
        {
            size_t i;
            for(i = 0; i < 15 && out[i] == "/proc/self/exe"[i]; i++);
            if(i == 15)
            {
                for(i = 0; i < MAX_PATH_LEN && exe_path[i]; i++)
                    out[i] = exe_path[i];
                if(i == MAX_PATH_LEN)
                    return -ENOMEM;
                out[i] = 0;
            }
        }
        if(binding)
            *binding = b;
    }
    if(!*out)
    {
        out[0] = '.';
        out[1] = 0;
    }
    return 0;
}

int translate_path(Path out, int dirfd, const char* path)
{
    return translate_path_internal(out, dirfd, path, 0, 0);
}

int strip_path(Path out, const char* base, const char* rel)
{
    if(*base != '/')
    {
        size_t i;
        for(i = 0; i < MAX_PATH_LEN && (!i || rel[i-1]); i++)
            out[i] = rel[i];
        if(i == MAX_PATH_LEN)
            return -ENOMEM;
        return 0;
    }
    size_t depth = 0;
    for(size_t i = 0; base[i]; i++)
        if(base[i] == '/')
            depth++;
    size_t i, j;
    for(i = 0, j = 0; rel[i] && j < MAX_PATH_LEN;)
    {
        if((i == 0 || rel[i-1] == '/') && rel[i] == '.' && rel[i+1] == '.' && (!rel[i+2] || rel[i+2] == '/'))
        {
            if(depth == 1)
            {
                i += 2;
                depth--;
                continue;
            }
            depth -= 2;
        }
        else if((i == 0 || rel[i-1] == '/') && rel[i] == '.' && (!rel[i+1] || rel[i+1] == '/'))
            depth--;
        out[j++] = rel[i];
        if(rel[i++] == '/' && !(j > 1 && out[j-2] == '/'))
            depth++;
    }
    if(j == MAX_PATH_LEN)
        return -ENOMEM;
    out[j] = 0;
    if(*rel && !*out)
    {
        out[0] = '.';
        out[1] = 0;
    }
    return 0;
}

static void hook_vfs(struct ucontext* ctx, int which, int flags)
{
    int error;
    Path path;
    if((error = translate_path_internal(path, AT_FDCWD, (const char*)ctx->ARG0, (flags & O_NOFOLLOW) ? TRPTH_NO_PROC_SELF_EXE : 0, 0)))
        ctx->RET = error;
    else
        ctx->RET = do_syscall(which, path, ctx->ARG1, ctx->ARG2, ctx->ARG3, ctx->ARG4, ctx->ARG5);
}

#define HOOK_VFS(name, flags) void name(struct ucontext* ctx, int which) { hook_vfs(ctx, which, flags); }

HOOK_VFS(hook_vfs0, 0)
HOOK_VFS(hook_vfs1, ctx->ARG1)
HOOK_VFS(hook_vfs2, ctx->ARG2)
HOOK_VFS(hook_vfs3, ctx->ARG3)
HOOK_VFS(hook_vfs4, ctx->ARG4)
HOOK_VFS(hook_vfs5, ctx->ARG5)
HOOK_VFS(hook_vfsl, O_NOFOLLOW)

static void hook_vfsat(struct ucontext* ctx, int which, int flags)
{
    int error;
    Path path;
    if((error = translate_path_internal(path, ctx->ARG0, (const char*)ctx->ARG1, (flags) ? TRPTH_NO_PROC_SELF_EXE : 0, 0)))
        ctx->RET = error;
    else
        ctx->RET = do_syscall(which, ctx->ARG0, path, ctx->ARG2, ctx->ARG3, ctx->ARG4, ctx->ARG5);
}

#define HOOK_VFSAT(name, flags) void name(struct ucontext* ctx, int which) { hook_vfsat(ctx, which, flags); }

HOOK_VFSAT(hook_vfsat0, 0)
HOOK_VFSAT(hook_openat, ctx->ARG2 & O_NOFOLLOW)
HOOK_VFSAT(hook_vfsat2, ctx->ARG2 & AT_SYMLINK_NOFOLLOW)
HOOK_VFSAT(hook_vfsat3, ctx->ARG3 & AT_SYMLINK_NOFOLLOW)
HOOK_VFSAT(hook_vfsat4, ctx->ARG4 & AT_SYMLINK_NOFOLLOW)
HOOK_VFSAT(hook_vfsat5, ctx->ARG5 & AT_SYMLINK_NOFOLLOW)
HOOK_VFSAT(hook_vfsatl, 1)

int vfs_openat(int dirfd, const char* pathname, int flags)
{
    int error;
    Path path;
    if((error = translate_path_internal(path, dirfd, pathname, (flags & O_NOFOLLOW) ? TRPTH_NO_PROC_SELF_EXE : 0, 0)))
        return error;
    return do_syscall(SYS_openat, dirfd, path, flags);
}

void hook_statx(struct ucontext* ctx, int which)
{
    if(have_statx)
        hook_vfsat2(ctx, which);
    else
        ctx->RET = -ENOSYS;
}

void hook_chown(struct ucontext* ctx, int which)
{
    hook_vfs0(ctx, which);
    if((uintptr_t)ctx->RET == (uintptr_t)-EPERM && fake_id0)
        ctx->RET = 0;
}

void hook_lchown(struct ucontext* ctx, int which)
{
    hook_vfsl(ctx, which);
    if((uintptr_t)ctx->RET == (uintptr_t)-EPERM && fake_id0)
        ctx->RET = 0;
}

void hook_fchownat(struct ucontext* ctx, int which)
{
    const char* path = (const char*)ctx->ARG1;
    if(!*path)
        ctx->RET = do_syscall(SYS_fchownat, ctx->ARG0, ctx->ARG1, ctx->ARG2, ctx->ARG3, ctx->ARG4);
    else
        hook_vfsat(ctx, which, ctx->ARG4);
    if((uintptr_t)ctx->RET == (uintptr_t)-EPERM && fake_id0)
        ctx->RET = 0;
}

void hook_getcwd(struct ucontext* ctx, int which)
{
    char* buf = (char*)ctx->ARG0;
    size_t sz = ctx->ARG1;
    Path path;
    ssize_t sz1 = do_syscall(SYS_getcwd, path, MAX_PATH_LEN);
    if(sz1 < 0)
        ctx->RET = sz1;
    else
    {
        int status;
        ctx->RET = translate_back(path, buf, sz, &status);
    }
}

void hook_rename_link(struct ucontext* ctx, int which)
{
    const char* orig_p1 = (const char*)ctx->ARG0;
    const char* orig_p2 = (const char*)ctx->ARG1;
    Path p1, p2;
    int error;
    if((error = translate_path(p1, AT_FDCWD, orig_p1))
    || (error = translate_path(p2, AT_FDCWD, orig_p2)))
    {
        ctx->RET = error;
        return;
    }
    ctx->RET = do_syscall(which, p1, p2);
}

void hook_symlink(struct ucontext* ctx, int which)
{
    Path target_path;
    Path host_path;
    const char* orig_dst = (const char*)ctx->ARG0;
    int dirfd = (which == SYS_symlinkat) ? ctx->ARG1 : AT_FDCWD;
    const char* orig_src = (const char*)((which == SYS_symlinkat) ? ctx->ARG2 : ctx->ARG1);
    if(*orig_dst == '/')
    {
#ifdef COMPAT_SYSCALLS
        if(which == SYS_symlink)
            hook_rename_link(ctx, SYS_symlink);
        else
#endif
        {
            int error;
            if((error = translate_path_internal(target_path, AT_FDCWD, orig_dst, TRPTH_NO_PROC_SELF_EXE, 0))
            || (error = translate_path(host_path, dirfd, orig_src)))
                ctx->RET = error;
            else
                ctx->RET = do_syscall(SYS_symlinkat, target_path, dirfd, host_path);
        }
        return;
    }
    int error;
    if((error = translate_path_internal(target_path, dirfd, orig_src, TRPTH_GUEST_ROOT, 0)))
        goto error_return;
    Path stripped_rel;
    if((error = strip_path(stripped_rel, target_path, orig_dst)))
        goto error_return;
    if((error = translate_path(host_path, dirfd, target_path)))
        goto error_return;
#ifdef COMPAT_SYSCALLS
    if(which == SYS_symlink)
        ctx->RET = do_syscall(SYS_symlink, stripped_rel, host_path);
    else
#endif
        ctx->RET = do_syscall(SYS_symlinkat, stripped_rel, dirfd, host_path);
    return;
error_return:
    ctx->RET = error;
}

void hook_readlink(struct ucontext* ctx, int which)
{
    Path pl, pt;
    int error;
    int dirfd = (which == SYS_readlinkat) ? ctx->ARG0 : AT_FDCWD;
    const char* path = (const char*)((which == SYS_readlinkat) ? ctx->ARG1 : ctx->ARG0);
    char* dst = (char*)((which == SYS_readlinkat) ? ctx->ARG2 : ctx->ARG1);
    size_t bufsz = (which == SYS_readlinkat) ? ctx->ARG3 : ctx->ARG2;
    if(*path)
        error = translate_path_internal(pl, dirfd, path, TRPTH_NO_PROC_SELF_EXE, 0);
    else
    {
        *pl = 0;
        error = 0;
    }
    size_t p;
    for(p = 0; p < 15 && pl[p] == "/proc/self/exe"[p]; p++);
    if(p == 15)
    {
        int status;
        ctx->RET = translate_back(exe_path, dst, bufsz, &status);
        if(ctx->RET > 0 && !dst[ctx->RET-1])
            ctx->RET--;
        return;
    }
    if(error
#ifdef COMPAT_SYSCALLS
    || (error = do_syscall(SYS_readlink, pl, pt, MAX_PATH_LEN)) < 0
#else
    || (error = do_syscall(SYS_readlinkat, AT_FDCWD, pl, pt, MAX_PATH_LEN)) < 0
#endif
    )
        ctx->RET = error;
    else if(*pt != '/')
    {
        if(bufsz > error)
            bufsz = error;
        for(size_t i = 0; i < bufsz; i++)
            dst[i] = pt[i];
        ctx->RET = bufsz;
    }
    else
    {
        if(error < MAX_PATH_LEN)
            pt[error] = 0;
        int status;
        ctx->RET = translate_back(pt, dst, bufsz, &status);
        if(ctx->RET > 0 && !dst[ctx->RET-1])
            ctx->RET--;
    }
}

void hook_rename_link_at(struct ucontext* ctx, int which)
{
    const char* orig_p1 = (const char*)ctx->ARG1;
    const char* orig_p2 = (const char*)ctx->ARG3;
    Path p1, p2;
    int error;
    const char* p_p1;
    if((ctx->ARG4 & AT_EMPTY_PATH))
        p_p1 = orig_p1;
    else if((error = translate_path_internal(p1, ctx->ARG0, orig_p1, (ctx->ARG4 & AT_SYMLINK_FOLLOW) ? 0 : TRPTH_NO_PROC_SELF_EXE, 0)))
    {
        ctx->RET = error;
        return;
    }
    if((error = translate_path_internal(p2, ctx->ARG2, orig_p2, (ctx->ARG4 & AT_SYMLINK_FOLLOW) ? 0 : TRPTH_NO_PROC_SELF_EXE, 0)))
    {
        ctx->RET = error;
        return;
    }
    ctx->RET = do_syscall(which, ctx->ARG0, p1, ctx->ARG2, p2, ctx->ARG4);
}

void hook_fchmodat(struct ucontext* ctx, int which)
{
    const char* path = (const char*)ctx->ARG1;
    if(!*path)
        ctx->RET = do_syscall(SYS_fchmodat, ctx->ARG0, ctx->ARG1, ctx->ARG2, ctx->ARG3);
    else
        hook_vfsat(ctx, which, ctx->ARG3);
}

void hook_statfs(struct ucontext* ctx, int which)
{
    int error;
    Path path;
    struct vfs_binding* binding;
    if((error = translate_path_internal(path, AT_FDCWD, (const char*)ctx->ARG0, 0, &binding)))
        ctx->RET = error;
    else
    {
        ctx->RET = do_syscall(which, path, ctx->ARG1);
        if(!ctx->RET)
        {
            int fake_tmpfs = 0;
            for(size_t i = 0; binding->flags[i]; i++)
                if(binding->flags[i] == 't')
                    fake_tmpfs = 1;
            if(fake_tmpfs)
                ((struct statfs*)ctx->ARG1)->f_type = TMPFS_MAGIC;
        }
    }
}

#ifndef IS_32BIT
void hook_newfstatat(struct ucontext* ctx, int which)
{
    int error;
    Path path;
    if((ctx->ARG3 & AT_EMPTY_PATH))
        ctx->RET = do_syscall(SYS_newfstatat, ctx->ARG0, ctx->ARG1, ctx->ARG2, ctx->ARG3);
    else if(!ctx->ARG1) //firefox calls it that way
        ctx->RET = -EFAULT;
    else if((error = translate_path_internal(path, ctx->ARG0, (const char*)ctx->ARG1, (ctx->ARG3 & AT_SYMLINK_NOFOLLOW) ? TRPTH_NO_PROC_SELF_EXE : 0, 0)))
        ctx->RET = error;
    else
        ctx->RET = do_syscall(SYS_newfstatat, ctx->ARG0, path, ctx->ARG2, ctx->ARG3);
}
#endif

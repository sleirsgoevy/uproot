#pragma once
#include <stdint.h>
#include <asm/sigcontext.h>
#include <asm/signal.h>
#if defined(__arm__) && !defined(__arm64__)
#include <asm-generic/ucontext.h>
#else
#include <asm/ucontext.h>
#endif

struct vfs_binding
{
    const char* host_path;
    const char* guest_path;
    const char* flags;
};

extern int have_statx;
extern const char* bindings_str;
extern struct vfs_binding* bindings;
void parse_bindings(const char* arg);
extern int fake_id0;

extern const char* exe_path;

typedef void syscall_hook(struct ucontext*, int which);

syscall_hook hook_vfs0;
syscall_hook hook_vfs1;
syscall_hook hook_vfs2;
syscall_hook hook_vfs3;
syscall_hook hook_vfs4;
syscall_hook hook_vfs5;
syscall_hook hook_vfsl;
syscall_hook hook_vfsat0;
syscall_hook hook_openat;
syscall_hook hook_vfsat2;
syscall_hook hook_vfsat3;
syscall_hook hook_vfsat4;
syscall_hook hook_vfsat5;
syscall_hook hook_vfsatl;
syscall_hook hook_statfs;
syscall_hook hook_getcwd;
syscall_hook hook_rename_link;
syscall_hook hook_symlink;
syscall_hook hook_readlink;
syscall_hook hook_newfstatat;
syscall_hook hook_rename_link_at;
syscall_hook hook_chown;
syscall_hook hook_lchown;
syscall_hook hook_fchownat;
syscall_hook hook_fchmodat;
syscall_hook hook_socket;
syscall_hook hook_connect;
syscall_hook hook_accept;
syscall_hook hook_bind;
syscall_hook hook_execve;
syscall_hook hook_execveat;
syscall_hook hook_statx;
syscall_hook hook_setxid;
syscall_hook hook_getxid;
syscall_hook hook_reboot;
syscall_hook hook_setrexid;
syscall_hook hook_setresxid;
syscall_hook hook_getresxid;
syscall_hook hook_fake_success;
syscall_hook hook_rt_sigprocmask;

#define MAX_PATH_LEN 4096
typedef char Path[MAX_PATH_LEN];

intptr_t do_syscall(int nr, ...);
int vfs_openat(int dirfd, const char* path, int flags);
void outn(char** out, unsigned int n);
void continue_execve(uintptr_t* stack, void* rdx, void* bpf);
int translate_path(Path out, int dirfd, const char* filename);

#if defined(__x86_64__)
#define COMPAT_SYSCALLS
#define ARG0 uc_mcontext.rdi
#define ARG1 uc_mcontext.rsi
#define ARG2 uc_mcontext.rdx
#define ARG3 uc_mcontext.r10
#define ARG4 uc_mcontext.r8
#define ARG5 uc_mcontext.r9
#define RET uc_mcontext.rax
#define EM_NATIVE EM_X86_64
#define AUDIT_ARCH_NATIVE AUDIT_ARCH_X86_64
#elif defined(__aarch64__)
#define ARG0 uc_mcontext.regs[0]
#define ARG1 uc_mcontext.regs[1]
#define ARG2 uc_mcontext.regs[2]
#define ARG3 uc_mcontext.regs[3]
#define ARG4 uc_mcontext.regs[4]
#define ARG5 uc_mcontext.regs[5]
#define RET uc_mcontext.regs[0]
#define EM_NATIVE EM_AARCH64
#define AUDIT_ARCH_NATIVE AUDIT_ARCH_AARCH64
#define PAGE_SIZE 4096 //XXX: 2048?
#elif defined(__arm__)
#define IS_32BIT
#define COMPAT_SYSCALLS
#define ARG0 uc_mcontext.arm_r0
#define ARG1 uc_mcontext.arm_r1
#define ARG2 uc_mcontext.arm_r2
#define ARG3 uc_mcontext.arm_r3
#define ARG4 uc_mcontext.arm_r4
#define ARG5 uc_mcontext.arm_r5
#define RET uc_mcontext.arm_r0
#define EM_NATIVE EM_ARM
#define AUDIT_ARCH_NATIVE AUDIT_ARCH_ARM
#define PAGE_SIZE 4096 //XXX: 2048?
#else
#error "unsupported architecture"
#endif

#ifdef IS_32BIT
#define SYS_mmap SYS_mmap2
#define ElfNative_Ehdr Elf32_Ehdr
#define ElfNative_Phdr Elf32_Phdr
#define ELFCLASSNATIVE ELFCLASS32
#else
#define ElfNative_Ehdr Elf64_Ehdr
#define ElfNative_Phdr Elf64_Phdr
#define ELFCLASSNATIVE ELFCLASS64
#endif

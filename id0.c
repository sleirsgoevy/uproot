#include <unistd.h>
#include <fcntl.h>
#include <sys/syscall.h>
#include <sys/errno.h>
#include "payload.h"

int fake_id0;

void hook_setxid(struct ucontext* ctx, int which)
{
    uint64_t target = ctx->ARG0;
    int getter;
    switch(which)
    {
    case SYS_setuid: getter = SYS_getuid; break;
    case SYS_setgid: getter = SYS_getgid; break;
    }
    uint64_t real = do_syscall(getter);
    ctx->RET = (target == real || (fake_id0 && !target)) ? 0 : -EPERM;
    if(!ctx->RET && target)
        fake_id0 = 0;
}

void hook_setrexid(struct ucontext* ctx, int which)
{
    uint64_t new_real = ctx->ARG0;
    uint64_t new_effective = ctx->ARG1;
    uid_t real, effective, saved;
    int getter;
    switch(which)
    {
    case SYS_setreuid: getter = SYS_getresuid; break;
    case SYS_setregid: getter = SYS_getresgid; break;
    }
    if(do_syscall(getter, &real, &effective, &saved)
    || (new_real != real && (!fake_id0 || new_real))
    || (new_effective != effective && (!fake_id0 || new_effective)))
        ctx->RET = -EPERM;
    else
    {
        ctx->RET = 0;
        if(new_effective)
            fake_id0 = 0;
    }
}

void hook_setresxid(struct ucontext* ctx, int which)
{
    uint64_t new_real = ctx->ARG0;
    uint64_t new_effective = ctx->ARG1;
    uint64_t new_saved = ctx->ARG1;
    uid_t real, effective, saved;
    int getter;
    switch(which)
    {
    case SYS_setresuid: getter = SYS_getresuid; break;
    case SYS_setresgid: getter = SYS_getresgid; break;
    }
    if(do_syscall(getter, &real, &effective, &saved)
    || (new_real != real && (!fake_id0 || new_real))
    || (new_effective != effective && (!fake_id0 || new_effective))
    || (new_saved != saved && (!fake_id0 || new_saved)))
        ctx->RET = -EPERM;
    else
    {
        ctx->RET = 0;
        if(new_effective)
            fake_id0 = 0;
    }
}

void hook_getxid(struct ucontext* ctx, int which)
{
    if(fake_id0)
        ctx->RET = 0;
    else
        ctx->RET = do_syscall(which);
}

void hook_getresxid(struct ucontext* ctx, int which)
{
    if(fake_id0)
    {
        *(uid_t*)ctx->ARG0 = 0;
        *(uid_t*)ctx->ARG1 = 0;
        *(uid_t*)ctx->ARG2 = 0;
        ctx->RET = 0;
    }
    else
        ctx->RET = do_syscall(which, ctx->ARG0, ctx->ARG1, ctx->ARG2);
}

void hook_reboot(struct ucontext* ctx, int which)
{
    if((uint32_t)ctx->ARG0 == 0xfee11d00)
    {
        fake_id0 = 1;
        ctx->RET = 0;
    }
    else
        ctx->RET = -EPERM;
}
